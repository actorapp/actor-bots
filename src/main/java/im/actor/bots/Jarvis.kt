package im.actor.bots

import im.actor.bots.framework.MagicForkScope
import im.actor.bots.framework.stateful.MagicStatefulBot
import im.actor.bots.framework.stateful.isText
import im.actor.bots.framework.stateful.oneShot
import im.actor.bots.framework.stateful.text
import im.actor.bots.framework.traits.aiQuery

class JarvisBot(scope: MagicForkScope) : MagicStatefulBot(scope) {

    override fun configure() {

        // Configure acting in groups
        enableInGroups = true
        ownNickname = "jarvis"
        onlyWithMentions = false

        // Configure AI
        aiSubscriptionKey = "80ec2051-9abf-48de-93c8-afb9d92f747b"
        registerAiAgent("default", "ru", "9e4f52f34b2c49b9aed24e551c56d9ab")

        // AI logic
        oneShot("default") {
            if (isText) {
                aiQuery(text) {
                    when (action) {
                        "tasks.suggest" -> {
                            sendText("$speech")
                        }
                    }
                }
            }
        }
    }
}