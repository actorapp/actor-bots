package im.actor.bots

import im.actor.bots.framework.MagicForkScope
import im.actor.bots.framework.stateful.*
import im.actor.bots.framework.traits.aiQuery

class ActorBot(scope: MagicForkScope) : MagicStatefulBot(scope) {

    override fun configure() {

        // Configure acting in groups
        enableInGroups = true
        ownNickname = "samantha"
        onlyWithMentions = true

        aiSubscriptionKey = "80ec2051-9abf-48de-93c8-afb9d92f747b"
        registerAiAgent("default", "en", "3cd13dea4fc244e297b71d35ea068651")

        // AI logic
        oneShot("default") {
            if (isText) {
                aiQuery(text) {
                    when (action) {
                        "notifications.add" -> {
                            if (pSummary == null) {
                                sendText("I am listening. ($raw)")
                            } else {
                                sendText("Ok! Creating reminder $pSummary ($raw)")
                            }
                        }
                        else -> {
                            if (speech != null) {
                                sendText("$speech ($raw)")
                            } else {
                                sendText("I don't know what to say you on this. ($raw)")
                            }
                        }
                    }
                }
            }
        }
    }
}