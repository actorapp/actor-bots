//package im.actor.bots
//
//import im.actor.bots.BotMessages
//import im.actor.bots.framework.magic.MagicCommandFork
//import im.actor.bots.framework.magic.MagicForkBot
//import im.actor.bots.framework.magic.MagicForkBotContext
//import org.apache.commons.io.IOUtils
//import org.apache.http.client.methods.HttpGet
//import org.apache.http.client.methods.HttpPost
//import org.apache.http.client.utils.URIBuilder
//import org.apache.http.entity.ContentType
//import org.apache.http.entity.StringEntity
//import org.apache.http.impl.client.HttpClients
//import org.apache.http.util.EntityUtils
//import org.jdom2.input.DOMBuilder
//import org.json.JSONObject
//import java.net.URLEncoder
//import java.util.*
//import javax.xml.parsers.DocumentBuilderFactory
//
///**
// * Translator bot
// */
//@Deprecated("Need to be replaced with new framework")
//class Translator(botContext: MagicForkBotContext) : MagicForkBot(botContext) {
//
//    lateinit var context: TranslatingContext
//
//    override fun preStart() {
//        super.preStart()
//
//        context = TranslatingContext()
//
//        startMessage = "Hi! I am here to help you with translating chat messages to up to ${context.supportedLanguages.count()} languages." +
//                "I can do it in two different modes: translating sentence when you explicitly ask" +
//                " me about this (with */translate* command) or auto translating all messages in conversation " +
//                "(with */start_translate* command).\n" +
//                "\n" +
//                "All supported commands:\n" +
//                "- */translate <text>* - translate <text> to english\n" +
//                "- */translate(language) <text>* - translate <text> to specific language where 'language' is specific language code\n" +
//                "- */translate_start* - starting translating every message to english\n" +
//                "- */translate_start(language)* - starting translating every message to 'language'\n" +
//                "- */translate_dialog(my,foreign)* - starting to translate with my messages to foreign language and opposite\n" +
//                "- */translate_langs* - list all available languages"
//
//        val clientId = serverKeyValue.getStringValue("clientId")
//        val clientSecret = serverKeyValue.getStringValue("clientSecret")
//
//        if (clientId != null && clientSecret != null) {
//            context.engine = tryCreateTranslator(clientId, clientSecret)
//        }
//
//        checkEngine()
//    }
//
//    override fun onReceiveCommand(command: String, args: Array<out String>, text: String, sender: BotMessages.UserOutPeer): Boolean {
//
//        val lang: String
//        when (args.count()) {
//            1 -> lang = args[0]
//            2 -> lang = args[0]
//            else -> lang = "en"
//        }
//
//        if (!context.supportedLanguages.contains(lang)) {
//            sendTextMessage("Unknown language code *$lang*. Please, type /translate_langs to list all available languages.")
//            return true
//        }
//
//        when (command) {
//            "translate" -> sendTextMessage(context.engine!!.translate(text, lang))
//            "translate_start" -> fork(ContinuousTranslation::class.java, context, lang)
//            "translate_langs" -> {
//                val langs = context.supportedLanguages.toList().sortedBy { a -> a.second }
//                var message = "Available languages and it's codes:"
//                for (i in langs) {
//                    message += "\n- ${i.second} - *${i.first}*"
//                }
//                sendTextMessage(message)
//            }
//            "translate_dialog" -> {
//                if (args.count() != 2) {
//                    sendTextMessage("You need to specify two arguments! Please, try again")
//                    return true
//                }
//                if (!context.supportedLanguages.contains(args[1])) {
//                    sendTextMessage("Unknown language code *${args[1]}*. Please, type /translate_langs to list all available languages.")
//                    return true
//                }
//
//                fork(DialogTranslation::class.java, context, sender.id(), lang, args[1])
//            }
//            "translate_help" -> sendTextMessage(startMessage)
//            else -> return false
//        }
//
//        return true
//    }
//
//
//    override fun onForkClosed() {
//        checkEngine()
//    }
//
//    fun checkEngine() {
//        if (context.engine == null) {
//            fork(NotRegisteredFork::class.java, context)
//        }
//    }
//
//    class ContinuousTranslation(val context: TranslatingContext,
//                                val lang: String,
//                                botContext: MagicForkBotContext) : MagicForkBot(botContext) {
//
//        init {
//            welcomeMessage = "Success! I will translate everything you write to ${context.supportedLanguages[lang]} language until you send /cancel to stop."
//        }
//
//        override fun onReceiveText(text: String, sender: BotMessages.UserOutPeer) {
//            sendTextMessage(context.engine!!.translate(text, lang))
//        }
//
//        override fun onReceiveCommand(command: String, args: Array<out String>, text: String, sender: BotMessages.UserOutPeer): Boolean {
//            return false
//        }
//
//        override fun onCancelled() {
//            super.onCancelled()
//
//            sendTextMessage("Good. Stopping translation.")
//        }
//    }
//
//    class DialogTranslation(val context: TranslatingContext,
//                            val ownId: Int,
//                            val ownLang: String,
//                            val foreignLang: String,
//                            botContext: MagicForkBotContext) : MagicForkBot(botContext) {
//
//        init {
//            welcomeMessage = "Success! I will translate ${context.supportedLanguages[ownLang]} <-> ${context.supportedLanguages[foreignLang]} until you send /cancel to stop."
//        }
//
//        override fun onReceiveText(text: String, sender: BotMessages.UserOutPeer) {
//            if (sender.id() == ownId) {
//                sendTextMessage(context.engine!!.translate(text, foreignLang))
//            } else {
//                sendTextMessage(context.engine!!.translate(text, ownLang))
//            }
//        }
//
//        override fun onReceiveCommand(command: String, args: Array<out String>, text: String, sender: BotMessages.UserOutPeer): Boolean {
//            return false
//        }
//
//        override fun onCancelled() {
//            super.onCancelled()
//
//            sendTextMessage("Good. Stopping translation.")
//        }
//    }
//
//    class NotRegisteredFork(val context: TranslatingContext,
//                            botContext: MagicForkBotContext) :
//            MagicForkBot(botContext) {
//        override fun onReceiveCommand(command: String, args: Array<out String>, text: String, sender: BotMessages.UserOutPeer): Boolean {
//            when (command) {
//                "translate_configure" -> {
//                    fork(RegisterCommand::class.java, context)
//                    return true
//                }
//                else -> {
//                    sendTextMessage("Before translation, you need to provide credentials. Senf */translate_configure* to start registration.")
//                    return false
//                }
//            }
//        }
//    }
//
//    class RegisterCommand(val context: TranslatingContext,
//                          botContext: MagicForkBotContext) :
//            MagicCommandFork("translate_configure", botContext) {
//
//        var clientId: String? = null
//        var clientSecret: String? = null
//
//        init {
//            welcomeMessage = "Translator Bot is not configured. Please provide required credentials to access Microsoft Translator API. First of all we need your Client Id."
//        }
//
//        override fun onStarted() {
//            super.onStarted()
//
//            // Resetting client
//            context.engine = null
//        }
//
//        override fun onReceiveText(text: String, sender: BotMessages.UserOutPeer) {
//            if (clientId == null) {
//                clientId = text
//                sendTextMessage("OK, ClientId '$clientId' set. Now Client Secret.")
//            } else if (clientSecret == null) {
//                clientSecret = text
//                val translator = tryCreateTranslator(clientId!!, clientSecret!!)
//                if (translator == null) {
//                    clientId = null
//                    clientSecret = null
//                    sendTextMessage("Unable to log in to Microsoft translator service.\nPlease, enter Client Id again or /cancel to abort registration.")
//                } else {
//                    serverKeyValue.setStringValue("clientId", clientId)
//                    serverKeyValue.setStringValue("clientSecret", clientSecret)
//                    context.engine = translator
//                    sendTextMessage("Success! Now you can to translations. Type /help for more information.")
//                    dismissFork()
//                }
//            }
//        }
//    }
//}
//
///**
// * Translation context
// */
//class TranslatingContext() {
//
//    val supportedLanguages = HashMap<String, String>()
//
//    init {
//        supportedLanguages.put("ar", "Arabic")
//        supportedLanguages.put("bs-Latn", "Bosnian (Latin)")
//        supportedLanguages.put("bg", "Bulgarian")
//        supportedLanguages.put("ca", "Catalan")
//        supportedLanguages.put("zh-CHS", "Chinese Simplified")
//        supportedLanguages.put("zh-CHT", "Chinese Traditional")
//        supportedLanguages.put("hr", "Croatian")
//        supportedLanguages.put("cs", "Czech")
//        supportedLanguages.put("da", "Danish")
//        supportedLanguages.put("nl", "Dutch")
//        supportedLanguages.put("en", "English")
//        supportedLanguages.put("et", "Estonian")
//        supportedLanguages.put("fi", "Finnish")
//        supportedLanguages.put("fr", "French")
//        supportedLanguages.put("de", "German")
//        supportedLanguages.put("el", "Greek")
//        supportedLanguages.put("ht", "Haitian Creole")
//        supportedLanguages.put("he", "Hebrew")
//        supportedLanguages.put("hi", "Hindi")
//        supportedLanguages.put("mww", "Hmong Daw")
//        supportedLanguages.put("hu", "Hungarian")
//        supportedLanguages.put("id", "Indonesian")
//        supportedLanguages.put("it", "Italian")
//        supportedLanguages.put("ja", "Japanese")
//        supportedLanguages.put("tlh", "Klingon")
//        supportedLanguages.put("tlh-Qaak", "Klingon (pIqaD)")
//        supportedLanguages.put("ko", "Korean")
//        supportedLanguages.put("lv", "Latvian")
//        supportedLanguages.put("lt", "Lithuanian")
//        supportedLanguages.put("ms", "Malay")
//        supportedLanguages.put("mt", "Maltese")
//        supportedLanguages.put("no", "Norwegian")
//        supportedLanguages.put("fa", "Persian")
//        supportedLanguages.put("pl", "Polish")
//        supportedLanguages.put("pt", "Portuguese")
//        supportedLanguages.put("otq", "Querétaro Otomi")
//        supportedLanguages.put("ro", "Romanian")
//        supportedLanguages.put("ru", "Russian")
//        supportedLanguages.put("sr-Cyrl", "Serbian (Cyrillic)")
//        supportedLanguages.put("sr-Latn", "Serbian (Latin)")
//        supportedLanguages.put("sk", "Slovak")
//        supportedLanguages.put("sl", "Slovenian")
//        supportedLanguages.put("es", "Spanish")
//        supportedLanguages.put("sv", "Swedish")
//        supportedLanguages.put("th", "Thai")
//        supportedLanguages.put("tr", "Turkish")
//        supportedLanguages.put("uk", "Ukrainian")
//        supportedLanguages.put("ur", "Urdu")
//        supportedLanguages.put("vi", "Vietnamese")
//        supportedLanguages.put("cy", "Welsh")
//        supportedLanguages.put("yua", "Yucatec Maya")
//    }
//
//    var engine: TranslateEngine? = null
//}
//
//
///**
// * Trying to create translator from client id and secret
// */
//fun tryCreateTranslator(clientId: String, clientSecret: String): TranslateEngine? {
//    val token = registerToken(clientId, clientSecret)
//    if (token != null) {
//        return TranslateEngine(clientId, clientSecret, token)
//    } else {
//        return null
//    }
//}
//
///**
// * Translating engine
// */
//class TranslateEngine(val clientId: String, val clientSecret: String, var token: String?) {
//
//    var tokenIssued = System.currentTimeMillis()
//
//    fun translate(source: String): String? {
//        return translate(source, "en")
//    }
//
//    fun translate(source: String, to: String): String? {
//        return translate(source, null, to)
//    }
//
//    fun translate(source: String, from: String?, to: String?): String? {
//
//        if (token == null || (System.currentTimeMillis() - tokenIssued!!) > 5 * 60 * 1000L) {
//            token = registerToken(clientId, clientSecret)!!
//            tokenIssued = System.currentTimeMillis()
//        }
//
//        // Building request
//        val urlBuilder = URIBuilder("http://api.microsofttranslator.com/v2/Http.svc/Translate")
//        urlBuilder.addParameter("text", source)
//        if (to != null) {
//            urlBuilder.addParameter("to", to)
//        } else {
//            urlBuilder.addParameter("to", "en")
//        }
//        val url = urlBuilder.build()
//
//        // Getting request
//        val client = HttpClients.createDefault()
//        val get = HttpGet(url)
//        get.addHeader("Authorization", "Bearer $token")
//        val response = client.execute(get).entity
//        var responseString = EntityUtils.toString(response, "UTF-8");
//
//        val docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
//        val doc = DOMBuilder().build(docBuilder.parse(IOUtils.toInputStream(responseString)))
//
//        return doc.rootElement.textNormalize
//    }
//}
//
///**
// * Getting access token from Azure Marketplace
// */
//private fun registerToken(clientId: String, clientSecret: String): String? {
//
//    val client = HttpClients.createDefault()
//    val post = HttpPost("https://datamarket.accesscontrol.windows.net/v2/OAuth2-13")
//    val cliendIdEncoded = URLEncoder.encode(clientId)
//    val cliendSecretEncoded = URLEncoder.encode(clientSecret)
//    post.entity = StringEntity("grant_type=client_credentials&client_id=$cliendIdEncoded&client_secret=$cliendSecretEncoded&scope=http://api.microsofttranslator.com", ContentType.APPLICATION_FORM_URLENCODED)
//
//    val response = client.execute(post).entity
//    var responseString = EntityUtils.toString(response, "UTF-8");
//    System.out.println(responseString)
//    val json = JSONObject(responseString)
//    if (json.has("access_token")) {
//        return json.getString("access_token")
//    } else {
//        return null;
//    }
//}