package im.actor.bots

import im.actor.bots.framework.MagicForkScope
import im.actor.bots.framework.stateful.*
import org.json.JSONObject

class GateKeeperBot(scope: MagicForkScope) : MagicStatefulBot(scope) {

    private val TOKEN_URL = "https://activation-gw.actor.im/v1/token"
    private val TOKEN_HEADER = "X-Super-Token"
    private val TOKEN_KEY = "a52dbe8eebc3bf84276b38fd8536374a55972f9179484acb7ab9fbe817ec0cfb6932920a9f85078f38d4d23917fcb"

    override fun configure() {

        enableSimpleMode("I can help you manage SMS gate for Actor. " +
                "For example, you can set *custom template* for SMS for your Server.\n\n" +
                "You can control me by sending these commands:\n" +
                "/newtoken - creating new gate token.\n" +
                "/list - Listing all your tokens.\n" +
                "/cancel - cancelling current action")

        //
        // Getting New Token
        //

        expectInput("/newtoken") {

            before {
                sendText("Please, send me template of your sms activation code. " +
                        "Use \$\$CODE\$\$ in place of code value. And Don't exceed 140 symbols.")
            }

            received {
                if (!isText) {
                    goto("main")
                    return@received
                }
                var items = JSONObject()
                items.put("template", text)
                items.put("user_id", scope.peer.id)
                val res = urlPostJson(TOKEN_URL, items, TOKEN_HEADER, TOKEN_KEY)
                if (res != null) {
                    val token = res.getString("token")
                    sendText("Your SMS Gate token is *$token*")
                    goto("main")
                } else {
                    sendText("Something went wrong. Unable to create token for you. Please, try again later.")
                }
            }

            validate {
                if (isText) {
                    if (text.length > 140) {
                        sendText("Your sms activation code is ${text.length} long and exceeds 140 symbols. Please, try again.")
                        return@validate false
                    } else if (!text.contains("\$\$CODE\$\$")) {
                        sendText("Your template doesn't contain \$\$CODE\$\$ placeholder. Please, try again.")
                        return@validate false
                    } else {
                        return@validate true
                    }
                } else if (isCommand) {
                    if (command == "cancel") {
                        return@validate true
                    }
                }
                sendText("Please, send me valid sms template.")
                return@validate false
            }
        }

        //
        // Listing tokens
        //

        oneShot("/list") {
            val res = urlGetJsonArray(TOKEN_URL + "?user_id=${scope.peer.id}", TOKEN_HEADER, TOKEN_KEY)
            if (res != null) {
                if (res.length() == 0) {
                    sendText("You don't have any active tokens")
                } else {
                    var message = ""
                    for (itm in res) {
                        val oitm = itm as JSONObject
                        if (!message.equals("")) {
                            message += "\n"
                        }
                        message += "*${oitm.getString("token")}*\n"
                        message += "- Template: ${oitm.getString("template")}"
                    }
                    sendText("Your tokens: \n $message\n")
                }
            } else {
                sendText("Oops, something went wrong. Please, try again later.")
            }
        }
    }
}