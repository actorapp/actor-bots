package im.actor.bots

import im.actor.bots.basic.NotificationOverlord
import im.actor.bots.framework.farm

fun main(args: Array<String>) {

    farm("NewFarm") {

        // Bot Father
        bot(BotFatherBot::class) {
            name = "botfather"
            token = "f6a10131f1fd6349557caeb1a0765267263ec20d"
            traceHook = "https://api.actor.im/v1/webhooks/88a3d0b4d78dce4693acfd2c6610e8b1f1e4531a4ffbfef16adb96b9d1436e59"
        }

        // Enterprise
        bot(EnterpriseBot::class) {
            name = "enterprise"
            token = "308e71e30eed917d22355cc7637b71a02238f002"
            traceHook = "https://api.actor.im/v1/webhooks/c866138fa60278c14668858e1dc0fcd51ddf20ae29c3e0b1fd113c71bf0780c3"
        }

        // Gate Keeper
        bot(GateKeeperBot::class) {
            name = "gatekeeper"
            token = "4777b0d0099aeab5388521946e722a4a77ff8c14"
            traceHook = "https://api.actor.im/v1/webhooks/70bb3fa21a77144120d4387020d374e031300278ed091aef7a2d68daeae0c09b"
        }

        // News
        bot(ActorNewsBot::class) {
            name = "news"
            token = "2cf1ef0de4dfaf93c4bfaa2c5703ea41c1936fca"
            overlordClazz = NotificationOverlord::class.java
        }

        // AI
        bot(ActorBot::class) {
            name = "Samantha"
            token = "6e19973e75591b68ca29cb9b993074e32726590b"
        }
        bot(JarvisBot::class) {
            name = "jarvis"
            token = "00720484d06cc4b77d1eb17033bd36226f5ec339"
        }

        // Fun
        bot(JokerBot::class) {
            name = "Jocker"
            token = "9aedcc302091d899671def5e91bd8705583dee4f"
        }
    }
}