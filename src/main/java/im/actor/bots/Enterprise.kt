package im.actor.bots

import im.actor.bots.framework.MagicForkScope
import im.actor.bots.framework.isEnterprise
import im.actor.bots.framework.stateful.*
import im.actor.bots.framework.traits.initLocalize
import org.json.JSONObject

class EnterpriseBot(scope: MagicForkScope) : MagicStatefulBot(scope) {

    override fun configure() {

        // Enabling bot state persistence
        enablePersistent = true

        // WARRING: Need always have any state created
        // Receiving JSON messages from Server
        static("default") {
            if (isJson && responseJson.optString("dataType") == "UserAuth") {
                tryGoto("user_auth")
                return@static
            }
            goto("main")
        }

        // Ignoring for non-private chats
        if (!scope.peer.isPrivate) {
            return
        }

        // Loading boarding user
        val user = getUser(scope.peer.id)

        // Not boarding if user is not enterprise
        if (!user.isEnterprise) {
            return
        }

        // Initialize i18n
        initLocalize("EnterpriseBot", arrayOf("ru", "en"), user.preferredLanguages.toTypedArray())

        // Getting email information
        val primaryEmail = user.emailContactRecords.first().email()
        val domain = primaryEmail.split("@").last().toLowerCase()

        // Entry point
        raw("user_auth") {

            before {

                // Check if boarding ended
                val boardingState = scope.botKeyValue.getJSONValue("boarding_${user.id()}")
                if (boardingState != null && boardingState.optBoolean("ended", false)) {
                    goto("main")
                    return@before
                }

                var company = scope.botKeyValue.getJSONValue("company_$domain")
                if (company != null && company.optString("title") != null) {

                    // If company created
                    sendText(localized("message.welcome"))
                    goto("check_company_group")
                } else {

                    // If company not created
                    sendText(localized("message.welcome") + "\n" +
                            localized("message.ask_company"))
                    goto("ask_company_name")
                }
            }

            expectInput("ask_company_name") {

                received {
                    scope.botKeyValue.setJSONValue("company_$domain", JSONObject().apply {
                        put("title", text)
                    })
                    sendText(localized("message.ask_company_set").replace("$(name)", text))
                    goto("check_company_group")
                }

                validate {

                    // Check Text
                    if (!isText) {
                        sendText(localized("message.ask_company_wrong"))
                        return@validate false
                    }

                    // Check length
                    if (text.length < 3 || text.length > 32) {
                        sendText(localized("message.ask_company_size"))
                        return@validate false
                    }

                    return@validate true
                }
            }

            raw("check_company_group") {
                before {

                    // Creating company group if necessary
                    var company = scope.botKeyValue.getJSONValue("company_$domain")!!
                    if (!company.has("group")) {
                        val group = createGroup(company.getString("title"))
                        if (group != null) {
                            company.put("group", JSONObject().apply {
                                put("group_id", group.peer().id())
                                put("group_hash", group.peer().accessHash())
                            })
                            scope.botKeyValue.setJSONValue("company_$domain", company)

                            // TODO: Implement Avatar Change
                        }
                    }

                    // Joining current user to group
                    var boarding = scope.botKeyValue.getJSONValue("boarding_${user.id()}")
                    if (boarding == null || !boarding.optBoolean("joinedGroup", false)) {
                        if (inviteUserToGroup(BotMessages.GroupOutPeer(company.getJSONObject("group").getInt("group_id"),
                                company.getJSONObject("group").getLong("group_hash")),
                                BotMessages.UserOutPeer(scope.sender.id(), scope.sender.accessHash()))) {

                            // Saving group joined state
                            if (boarding == null) {
                                boarding = JSONObject()
                            }
                            boarding.put("joinedGroup", true)
                            scope.botKeyValue.setJSONValue("boarding_${user.id()}", boarding)

                            sendText(localized("message.group_invite"))
                        }
                    }

                    // GoTo asking user's name
                    goto("ask_name")
                }
            }

            //
            // Asking User Name
            //

            expectInput("ask_name") {

                before {
                    sendText(localized("message.ask_name"))
                }

                received {
                    if (changeUserName(user.id(), text)) {
                        sendText(localized("message.ask_name_set").replace("$(name)", text))
                        goto("ask_photo")
                    } else {
                        sendText(localized("message.internal_error"))
                    }
                }

                validate {

                    if (isText && text.toLowerCase() == localized("message.no")) {
                        goto("complete")
                        return@validate false
                    }

                    if (!isText) {
                        sendText(localized("message.ask_name_wrong"))
                        return@validate false
                    }

                    // Check length
                    if (text.length < 3 || text.length > 32) {
                        sendText(localized("message.ask_name_size"))
                        return@validate false
                    }

                    return@validate true
                }
            }

            //
            // Asking User Photo
            //

            expectInput("ask_photo") {

                before {
                    sendText(localized("message.ask_photo"))
                }

                received {
                    if (changeUserAvatar(user.id(), doc.fileId(), doc.accessHash())) {
                        sendText(localized("message.ask_photo_set"))
                        goto("complete")
                    } else {
                        sendText(localized("message.internal_error"))
                    }
                }

                validate {

                    if (isText && text.toLowerCase() == localized("message.no")) {
                        goto("complete")
                        return@validate false
                    }

                    if (isDoc) {
                        if (isPhoto) {
                            return@validate true
                        } else {
                            sendText(localized("message.ask_photo_wrong_doc"))
                        }
                    } else {
                        sendText(localized("message.ask_photo_wrong"))
                    }
                    return@validate false
                }
            }

            //
            // Ending boarding
            //

            static("complete") {

                var boarding = scope.botKeyValue.getJSONValue("boarding_${user.id()}")
                if (boarding == null) {
                    boarding = JSONObject()
                }
                boarding!!.put("ended", true)
                scope.botKeyValue.setJSONValue("boarding_${user.id()}", boarding)

                sendText(localized("message.complete"))
                goto("main")
            }
        }
    }
}