package im.actor.bots

import im.actor.bots.basic.NotificationBot
import im.actor.bots.framework.MagicForkScope

class ActorNewsBot(scope: MagicForkScope) : NotificationBot(scope) {

    override fun configure() {
        ownNickname = "a_news"
        enableInGroups = true
        onlyWithMentions = false
        
        admins.add("steve")
        admins.add("prettynatty")

        welcomeMessage = "Hello! I am here to help you to get important notifications about Actor platform. " +
                "Just send me /subscribe and i will start sending you notifications once something important happened."

        super.configure()
    }
}