//package im.actor.bots
//
//import im.actor.bots.framework.MagicForkScope
//import im.actor.bots.framework.MagicOverlord
//import im.actor.bots.framework.MagicalRemoteBot
//import im.actor.bots.framework.MagicStatefulBot
//import im.actor.bots.framework.expects.isJson
//import im.actor.bots.framework.expects.oneShot
//import org.json.JSONObject
//import java.util.*
//
//class OAuthBot(scope: MagicForkScope) : MagicStatefulBot(scope) {
//
//    val CLIENT_ID = "o4ALAHGk"
//    val CLIENT_SECRET = "ImJAdc6mE4be4aNMgdpIJdsOoKCU1LcC9zcvRrSMFotF7GoHLqq32lVbtqRVsvou"
//
//    override fun configure() {
//
//        // Default message handler
//        oneShot("default") {
//            if (isJson) {
//
//            }
//        }
//
//        // Authentication command
//        oneShot("/auth") {
//            if (scope.peer.isGroup) {
//                sendText("Please, do this in private")
//            } else {
//                val rid = Math.abs(Random().nextLong())
//                val url = "https://www.wrike.com/oauth2/authorize?client_id=$CLIENT_ID&response_type=code&state=$rid"
//                sendText("Please, open $url")
//            }
//        }
//
//        // Admin
//        oneShot("/admin_get_callback") {
//            if (isAdmin(scope.sender)) {
//                if (scope.peer.isGroup) {
//                    sendText("Please, do this in private")
//                } else {
//                    var hook = scope.botKeyValue.getStringValue("oauth_hook")
//                    if (hook == null) {
//                        hook = createHook("oauth_callback")
//                        scope.botKeyValue.setStringValue("oauth_hook", hook)
//                    }
//                    sendText("Callback URL: $hook")
//                }
//            } else {
//                sendText("You is not allowed to perform operation")
//            }
//        }
//    }
//}
//
////class OAuthOverlord(bot: MagicalRemoteBot) : MagicOverlord(bot) {
////
////    override fun onWebHookReceived(name: String, body: ByteArray, headers: JSONObject) {
////        if (name == "oauth_callback") {
////
////        }
////    }
////}