package im.actor.bots

import im.actor.bots.framework.MagicForkScope
import im.actor.bots.framework.stateful.MagicStatefulBot
import im.actor.bots.framework.stateful.isText
import im.actor.bots.framework.stateful.oneShot
import im.actor.bots.framework.stateful.text

class JokerBot(scope: MagicForkScope) : MagicStatefulBot(scope) {

    override fun configure() {

        // Configure group behaviour
        ownNickname = "joker"
        enableInGroups = true
        onlyWithMentions = false

        oneShot("/advice") {
            var advice = urlGetJson("http://fucking-great-advice.ru/api/random")
            if (advice != null) {
                sendText(advice.getString("text").replace("&nbsp;", " "))
            }
        }
        oneShot("default") {
            if (isText) {
                if (text.toLowerCase() == "нет") {
                    sendText("Пидора ответ")
                }
            }
        }
    }
}