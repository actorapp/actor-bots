package im.actor.bots

import im.actor.bots.framework.i18n.Cancelling
import kotlin.test.*
import org.junit.Test
import org.junit.After
import org.junit.Before

public class HumanityTests {
    @Before fun setUp() {
        // set up the test case
    }

    @After fun tearDown() {
        // tear down the test case
    }

    @Test fun testCancellingRussian() {
        assert(Cancelling.isCancelling("Нет!"))
        assert(Cancelling.isCancelling("нет"))
        assert(Cancelling.isCancelling("  Нет!"))
        assert(Cancelling.isCancelling("  Нет!  "))
        assert(Cancelling.isCancelling("  Нет  "))
    }

    @Test fun testCancellingEnglish() {
        assert(Cancelling.isCancelling("no"))
        assert(Cancelling.isCancelling("nope"))
        assert(Cancelling.isCancelling("stop"))
        assert(Cancelling.isCancelling("cancel"))
    }
}